local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "cspider2p"

local assets = 
{
	Asset("ANIM", "anim/ds_spider_basic.zip"),
    Asset("ANIM", "anim/ds_spider_warrior.zip"),
    Asset("SOUND", "sound/spider.fsb"),
	
	Asset("ANIM", "anim/hider_shiny_build_01.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 300,
	hunger = 175,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 150,
	
	runspeed = 5,
	walkspeed = 5,
	
	attackperiod = 0,
	damage = 30,
	range = 3,
	hit_range = 3,
	
	bank = PP_SPIDER_HATS and "spider_hider_hats" or "spider_hider",
	build = "DS_spider_caves",
	shiny = "hider",
	
	scale = 1,
	stategraph = "SGcspiderp",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('cspider2p',
-----Prefab---------------------Chance------------
{
    {'monstermeat',			1.00},
})
local FORGE_STATS = PPC_FORGE.HIDER
--==============================================
--					Mob Functions
--==============================================

--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	

	
	if inst.components.shedder ~= nil then
		inst.components.shedder:StopShedding()
	end
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"staves", "books"})
	end)
	
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("spiderwhisperer")
	inst:AddTag("spider_hider")
	inst:AddTag("monster")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true, true) --mob table, ishuman, ignorepvpmultiplier, ignoreweb
	PlayablePets.SetCommonWeatherResistances(inst, nil, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, 0.5, 0.5) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.canhide = true
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeTinyFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .5)
	inst.Transform:SetFourFaced()
	inst.DynamicShadow:SetSize(1.5, .5)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", function(inst, data) PlayablePets.GetAllyHelp(inst, data.attacker, "spider") end)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)