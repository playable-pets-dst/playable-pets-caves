local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "hutchp"

local assets = 
{
	
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 300,
	hunger = 100,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 200,
	runspeed = 7,
	walkspeed = 6,
	damage = 25,
	attackperiod = 1,
	range = 2,
	hit_range = 2,
	bank = "hutch",
	build = "hutch_build",
	build2 = "hutch_musicbox_build",
	build3 = "hutch_pufferfish_build",
	scale = 1,
	stategraph = "SGhutchp", --could use the chesterp graph, but would require integration.
	minimap = "hutchp.png",
}

--Loot that drops when you die, duh.
SetSharedLootTable('hutchp',
-----Prefab---------------------Chance------------
{
    {'smallmeat',             1.00},
	{'smallmeat',             1.00},   
})

local FORGE_STATS = PPC_FORGE.HUTCH
--==============================================
--					Mob Functions
--==============================================
local sounds =
{
    sleep = "dontstarve/creatures/together/hutch/sleep",
    hurt = "dontstarve/creatures/together/hutch/hit",
    pant = "dontstarve/creatures/together/hutch/pant",
    death = "dontstarve/creatures/together/hutch/death",
    open = "dontstarve/creatures/together/hutch/open",
    close = "dontstarve/creatures/together/hutch/close",
    pop = "dontstarve/creatures/together/hutch/pop",
    lick = "dontstarve/creatures/together/hutch/lick",
    boing = "dontstarve/creatures/together/hutch/land_hit",
    land = "dontstarve/creatures/together/hutch/land",
    musicbox = "dontstarve/creatures/together/hutch/one_man_band",
}

--------------------------------------------------------------------------------
--Normal and Musicbox Hutch with lightbulb
local LIGHT_RADIUS = 2.5
local LIGHT_INTENSITY = .8
local LIGHT_FALLOFF = .4

--Puffy Hutch with lightbulb
local DIM_LIGHT_RADIUS = 2.1
local DIM_LIGHT_INTENSITY = .6
local DIM_LIGHT_FALLOFF = .55

--Musicbox Hutch with no lightbulb
local FAINT_LIGHT_RADIUS = 1
local FAINT_LIGHT_INTENSITY = .01
local FAINT_LIGHT_FALLOFF = .4

local NORMAL_LIGHT_COLOUR = { 180 / 255, 195 / 255, 150 / 255 }
local MUSIC_LIGHT_COLOUR = { 150 / 255, 150 / 255, 255 / 255 }

local function SetNormalLight(inst)
    inst.Light:SetRadius(LIGHT_RADIUS)
    inst.Light:SetIntensity(LIGHT_INTENSITY)
    inst.Light:SetFalloff(LIGHT_FALLOFF)
    inst.Light:SetColour(unpack(NORMAL_LIGHT_COLOUR))
end

local function SetDimLight(inst)
    inst.Light:SetRadius(DIM_LIGHT_RADIUS)
    inst.Light:SetIntensity(DIM_LIGHT_INTENSITY)
    inst.Light:SetFalloff(DIM_LIGHT_FALLOFF)
    inst.Light:SetColour(unpack(NORMAL_LIGHT_COLOUR))
end

local function SetMusicLight(inst)
    inst.Light:SetRadius(LIGHT_RADIUS)
    inst.Light:SetIntensity(LIGHT_INTENSITY)
    inst.Light:SetFalloff(LIGHT_FALLOFF)
    inst.Light:SetColour(unpack(MUSIC_LIGHT_COLOUR))
end

local function IsMusic(item)
	return item:HasTag("band") or nil
end

local function IsSpear(item)
	return item.prefab == "spear" or nil
end

local function IsLightbulb(item)
	return item.prefab == "lightbulb" or nil
end

--------------------------------------------------------------------------------

local function LightBattery(item)
    return item:HasTag("lightbattery")
end

local function FindBattery(inst, fn)
    return inst.components.inventory:FindItem(fn)
end

local function OnReflectDamage(inst, data)
	--local hands = inst.components.inventory:GetEquippedItem()
    local spear = inst.components.inventory:FindItem(IsSpear)
    if spear ~= nil and spear.components.finiteuses ~= nil then
        spear.components.finiteuses:Use(
            spear.components.weapon ~= nil and
            spear.components.weapon.attackwear or
            1
        )
    end
    if data.attacker ~= nil and data.attacker:IsValid() then
        local impactfx = SpawnPrefab("impact")
        if impactfx ~= nil then
            if data.attacker.components.combat ~= nil then
                local follower = impactfx.entity:AddFollower()
                follower:FollowSymbol(data.attacker.GUID, data.attacker.components.combat.hiteffectsymbol, 0, 0, 0)
            else
                impactfx.Transform:SetPosition(data.attacker.Transform:GetWorldPosition())
            end
            impactfx:FacePoint(inst.Transform:GetWorldPosition())
        end
    end
end

local function CreateGroundGlow(inst, rotrate)
    local groundglow = SpawnPrefab("hutch_music_light_fx")
    local scale = (math.random() * .2 + 1.2) * (math.random() < .5 and 1 or -1)
    groundglow.Transform:SetScale(scale, scale, scale)
    groundglow.Follower:FollowSymbol(inst.GUID, "base_point", 0, 0, 0)
    groundglow:InitFX(
        inst,
        {
            rot = math.random(0, 359),
            rotrate = rotrate,
            alpha = math.random(),
            alphadir = math.random() < .5,
            alpharate = math.random() * .02 + .005,
        }
    )
    return groundglow
end

local function CheckBattery(inst)
    local current_form = inst.components.amorphous:GetCurrentForm()

    local lightbattery = --[[ works for all forms ]] FindBattery(inst, LightBattery)
    local pointybattery = current_form == "FUGU" and FindBattery(inst, IsSpear) or nil
    local IsMusic = current_form == "MUSIC" and lightbattery ~= nil and FindBattery(inst, IsMusic) or nil

    if inst._lightbattery ~= lightbattery then
        if lightbattery ~= nil then
            if inst._lightbattery == nil then
                inst.Light:Enable(true)
                inst.AnimState:Show("fx_lure_light")
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/hutch/light_on")
            end
        elseif inst._lightbattery ~= nil then
           -- inst.SoundEmitter:PlaySound("dontstarve/creatures/together/hutch/light_off")
           -- inst.Light:Enable(true) --we always want light on
            --inst.AnimState:Hide("fx_lure_light")
        end
        inst._lightbattery = lightbattery
    end

    if inst._pointybattery ~= pointybattery then
        if pointybattery ~= nil then
            if inst._pointybattery == nil then
                inst:AddComponent("damagereflect")
                inst.components.damagereflect:SetDefaultDamage(TUNING.HUTCH_PRICKLY_DAMAGE)
                inst:ListenForEvent("onreflectdamage", OnReflectDamage)
            end
        elseif inst._pointybattery ~= nil then
            inst:RemoveComponent("damagereflect")
            inst:RemoveEventCallback("onreflectdamage", OnReflectDamage)
        end
        inst._pointybattery = pointybattery
    end

    if inst._IsMusic ~= IsMusic then
        if inst._IsMusic ~= nil and
            inst._IsMusic:IsValid() and
            inst._IsMusic.components.fueled ~= nil then
            inst._IsMusic.components.fueled:StopConsuming()
        end
        if IsMusic ~= nil then
            if inst._IsMusic == nil then
                inst:AddComponent("sanityaura")
                inst.components.sanityaura.aura = TUNING.SANITYAURA_MED
                inst.SoundEmitter:PlaySound(inst.sounds.musicbox, "hutchMusic")
                inst.SoundEmitter:SetParameter("hutchMusic", "intensity", inst.sg:HasTag("open") and 1 or 0)
                inst._groundglows =
                {
                    CreateGroundGlow(inst, .5),
                    CreateGroundGlow(inst, -.5),
                    CreateGroundGlow(inst, 1),
                }
            end
            if IsMusic.components.fueled ~= nil then
                IsMusic.components.fueled:StartConsuming()
            end
        elseif inst._IsMusic ~= nil then
            inst:RemoveComponent("sanityaura")
            inst.SoundEmitter:KillSound("hutchMusic")
            for i, v in ipairs(inst._groundglows) do
                v:Remove()
            end
            inst._groundglows = nil
        end
        inst._IsMusic = IsMusic
    end
end

local function MorphSpike(inst)
	if inst.isshiny == 0 then
    inst.AnimState:SetBuild(mob.build3)
	elseif inst.isshiny ~= 0 then
	inst.AnimState:SetBuild("hutch_pufferfish_shiny_build_0"..inst.isshiny)
	end
    inst:AddComponent("damagereflect")
    inst.components.damagereflect:SetDefaultDamage(TUNING.HUTCH_PRICKLY_DAMAGE)
    inst:ListenForEvent("onreflectdamage", OnReflectDamage)
	SetDimLight(inst)	
	inst.skinn = 1
end

local function MorphMusic(inst)
	if inst.isshiny == 0 then
		inst.AnimState:SetBuild(mob.build2)
	elseif inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("hutch_musicbox_shiny_build_0"..inst.isshiny)
	end
    --inst.MiniMapEntity:SetIcon("chestersnow.png")
	
	SetMusicLight(inst)
	inst:AddComponent("sanityaura")
    inst.components.sanityaura.aura = TUNING.SANITYAURA_MED
    inst.SoundEmitter:PlaySound(inst.sounds.musicbox, "hutchMusic")
    --inst.SoundEmitter:SetParameter("hutchMusic", "intensity", inst.sg:HasTag("open") and 1 or 0)
    inst._groundglows =
          {
               CreateGroundGlow(inst, .5),
               CreateGroundGlow(inst, -.5),
               CreateGroundGlow(inst, 1),
          }

    inst.skinn = 2
end

local function MorphNormal(inst)
	if inst.isshiny == 0 then
		inst.AnimState:SetBuild(mob.build)
	elseif inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("hutch_shiny_build_0"..inst.isshiny)
	end
	SetNormalLight(inst)
	inst:RemoveComponent("damagereflect")
    inst:RemoveEventCallback("onreflectdamage", OnReflectDamage)
	inst:RemoveComponent("sanityaura")
    inst.SoundEmitter:KillSound("hutchMusic")
	if inst._groundglows ~= nil then
    for i, v in ipairs(inst._groundglows) do
      v:Remove()
    end
	inst._groundglows = nil
	end
	inst.skinn = 0
end

--------------------------------------------------------------------


local function CreateForm(name, itemtags, build, icon, onenter, onexit)
    local function enterfn(inst)
        inst.AnimState:SetBuild(build)
        inst.MiniMapEntity:SetIcon(icon)
        inst.components.maprevealable:SetIcon(icon)
        if onenter ~= nil then
            onenter(inst)
        end
        CheckBattery(inst)
    end

    return
    {
        name = name,
        itemtags = itemtags,
        enterformfn = function(inst, instant)
            if not instant then
                inst:PushEvent("morph", { morphfn = enterfn })
            elseif enterfn ~= nil then
                enterfn(inst)
            end
        end,
        exitformfn = onexit,
    }
end

local forms =
{
    CreateForm("FUGU", { "lightbattery", "pointy" }, "hutch_pufferfish_build", "hutch_pufferfish.png", SetDimLight, nil),
    CreateForm("MUSIC", { "lightbattery", "band" }, "hutch_musicbox_build", "hutch_musicbox.png", SetMusicLight, nil),
    CreateForm("NORMAL", nil, "hutch_build", "hutch.png", SetNormalLight, nil),
}

CreateForm = nil

local function OnTransform(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
	local body = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY) or nil
	local musicbox = inst.components.inventory:FindItem(IsMusic) or nil
	local req = 9
	if inst.skinn > 0 then
		if hands == nil or not hands:HasTag("pointy") then
			if musicbox == nil then
				inst.skinn = 0
				MorphNormal(inst)
			end
		end
	end	
	if inst.skinn == 0 then
		if inst.components.inventory ~= nil and hands ~= nil and hands:HasTag("pointy") then
			if musicbox == nil then
			--inst.components.inventory:ConsumeByName("nightmarehands:HasTag("pointy")", 9)
			inst.skinn = 1
			
			inst.sg:GoToState("morph")
			end
		elseif inst.components.inventory ~= nil and musicbox ~= nil then
			if hands == nil or not hands:HasTag("pointy") then
			inst.skinn = 2
			inst.sg:GoToState("morph")
			end
		end
	end	
end
--==============================================
--				Custom Common Functions
--==============================================	
local function SkinSet(inst)
	if inst.skinn ~= nil and inst.skinn == 0 then
		inst.AnimState:SetBuild(mob.build)
	elseif inst.skinn ~= nil and inst.skinn == 1 then
		MorphSpike(inst)
	elseif inst.skinn ~= nil and inst.skinn == 2 then	
		MorphMusic(inst)
	end	
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--

	----------------------------------
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.isshiny = 0
	inst.skinn = 0
	inst.getskins = getskins
	inst.leave_slime = true
	
	inst.sounds = sounds
	
	inst.userfunctions =
	{
		MorphSpike = MorphSpike,
		MorphMusic = MorphMusic,
	}
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 75, 0.5)
    inst.DynamicShadow:SetSize(2, 1.5)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("equip", OnTransform) 
	inst:ListenForEvent("unequip", OnTransform) 
	inst:ListenForEvent("itemget", OnTransform)
	inst:ListenForEvent("itemlose", OnTransform)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst.entity:AddLight()
	inst.Light:Enable(true)
	SetNormalLight(inst)
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) SkinSet(inst) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) SkinSet(inst) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) inst.Light:Enable(true) SetNormalLight(inst) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)