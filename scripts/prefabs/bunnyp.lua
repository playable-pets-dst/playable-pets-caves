local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "bunnyp"

local assets = 
{
	Asset("ANIM", "anim/manrabbit_basic.zip"),
    Asset("ANIM", "anim/manrabbit_actions.zip"),
    Asset("ANIM", "anim/manrabbit_attacks.zip"),
    Asset("ANIM", "anim/manrabbit_build.zip"),
    Asset("ANIM", "anim/manrabbit_boat_jump.zip"),

    Asset("ANIM", "anim/manrabbit_beard_build.zip"),
    Asset("ANIM", "anim/manrabbit_beard_basic.zip"),
    Asset("ANIM", "anim/manrabbit_beard_actions.zip"),
    Asset("SOUND", "sound/bunnyman.fsb"),

	Asset("ANIM", "anim/manrabbit_shiny_build_01.zip"),
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {"1"}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 250,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --WILSON_HUNGER_RATE = .15625
	sanity = 175,
	runspeed = 5.50,
	walkspeed = 3,
	damage = 34,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	bank = "manrabbit",
	build = "manrabbit_build",
	scale = 1.25,
	build2 = "manrabbit_beard_build",
	stategraph2 = "SGbunnyp_evil",
	stategraph = "SGbunnyp",
	minimap = "bunnyp.tex",
}

--Loot that drops when you die, duh.
SetSharedLootTable('bunnyp',
-----Prefab---------------------Chance------------
{
    {'meat',			1.00},
	{'manrabbit_tail',			1.00},
	{'carrot',			1.0},
})

local FORGE_STATS = PPC_FORGE.BUNNYMAN
--==============================================
--					Mob Functions
--==============================================
local function ontalk(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/bunnyman/idle_med")
end

local function SetCurse(inst)
	inst.isbeardlord = true
    inst.AnimState:SetBuild(mob.build2)
	inst.components.locomotor.runspeed = (mob.runspeed + 2)
	inst.components.combat:SetDefaultDamage(mob.damage*1.5)
end

local function RemoveCurse(inst)
	inst.isbeardlord = false
	if inst.isshiny == 0 then
		inst.AnimState:SetBuild(mob.build)
	else
		inst.AnimState:SetBuild("manrabbit_shiny_build_0"..inst.isshiny)
	end
	inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.combat:SetDefaultDamage(mob.damage)
	inst.components.health:DoDelta(25, false)
end
--==============================================
--				Custom Common Functions
--==============================================	
local function IsCarrot(item)
    return item.prefab == "carrot" or item.prefab == "carrot_cooked"
end

local function OnEat(inst, food)
    if food and food.components.edible then
		if food.prefab == "carrot" or food.prefab == "carrot_cooked" then
			--inst.components.sanity:DoDelta(10, false)
			inst.components.hunger:DoDelta(10, false)
		end
    end
end

local BEARDLORD_TRIGGER = 0.5

local function HealthDrain(inst)
	inst.components.health:DoDelta(-2, false) --Might be better to use HealthRegen function?
end

local function CanTransform(inst)
	local sanity = inst.components.sanity:GetPercent()
	if sanity <= BEARDLORD_TRIGGER and inst.isbeardlord == false then
		SetCurse(inst)
		inst.beardlord_drain = inst:DoPeriodicTask(2, HealthDrain)
	elseif sanity > BEARDLORD_TRIGGER and inst.isbeardlord == true then
		RemoveCurse(inst)
		if inst.beardlord_drain then
			inst.beardlord_drain:Cancel()
			inst.beardlord_drain = nil
		end
	end	
end

local function SetSkinDefault(inst, num)
	if not inst.isbeardlord then --don't set skin if beardlord
		if num and num ~= 0 then
			inst.AnimState:SetBuild(mob.shiny.."_shiny_build_0"..num)
		else
			inst.AnimState:SetBuild(mob.build)
		end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("manrabbit") --PIGS HATE HIM!
	inst:AddTag("bunnyplayer")
    inst:AddTag("pig")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local function ApplyBuild(inst) --because CommonStats can't check for beardlord status
	inst.AnimState:SetBuild(inst.isbeardlord and mob.build2 or mob.build)
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.isbeardlord = false
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false
	inst.mobplayer = true
	inst.isshiny = 0
	inst.getskins = getskins
	inst.setskin_defaultfn = SetSkinDefault
	
	inst.userfunctions =
	{	
		SetCurse = SetCurse,
		RemoveCurse = RemoveCurse,	
	}
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }, { FOODTYPE.SEEDS, FOODTYPE.GENERIC, FOODTYPE.VEGGIE }) 
	inst.components.eater:SetOnEatFn(OnEat)
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.5, .5)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("sanitydelta", CanTransform)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	inst.components.talker.ontalk = ontalk
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) ApplyBuild(inst) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) ApplyBuild(inst) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)