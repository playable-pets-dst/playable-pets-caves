local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "guardianp"

local assets = 
{
	Asset("ANIM", "anim/rook.zip"),
    Asset("ANIM", "anim/rook_rhino.zip"),
    Asset("ANIM", "anim/rook_rhino_damaged_build.zip"),
    Asset("ANIM", "anim/rook_attacks.zip"),
	Asset("ANIM", "anim/rook_rhino_blood_big_fx.zip"),
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {"1"}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = BOSS_STATS and TUNING.MINOTAUR_HEALTH or 4000,
	hunger = 200,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --WILSON_HUNGER_RATE = .15625
	sanity = 150,
	runspeed = TUNING.MINOTAUR_RUN_SPEED,
	walkspeed = TUNING.MINOTAUR_WALK_SPEED,
	damage = 200,
	attackperiod = 3,
	range = 4.1-0.5,
	hit_range = 4.1+0.5,
	bank = "rook",
	build = "rook_rhino",
    build2 = "rook_rhino_damaged_build",
	shiny = "guardian",
	scale = 1,
	stategraph = "SGguardianp",
	minimap = "guardianp.tex",
}

--Loot that drops when you die, duh.
SetSharedLootTable('guardianp',
-----Prefab---------------------Chance------------
{
    {"meat",        1.00},
    {"meat",        1.00},
    {"meat",        1.00},
    {"meat",        1.00},
    {"meat",        1.00},
    {"meat",        1.00},
    {"meat",        1.00},
    {"meat",        1.00},
    {"minotaurhorn",1.00},
})

local FORGE_STATS = PPC_FORGE.GUARDIAN
--==============================================
--					Mob Functions
--==============================================
local PHASE1 = 0.6

local function IsChess(dude)
    return dude:HasTag("chess")
end

local function checkfortentacledrop(inst)
    if inst:IsValid() then
        local chance = Remap(inst.components.health:GetPercent(),0,PHASE1,0.05,0.01)
        if math.random() < chance then
            inst.SpawnBigBloodDrop(inst)
        end

        local chance2 = Remap(inst.components.health:GetPercent(),0,PHASE1,0.8,0.01)
        if math.random() < chance2 then
            inst.SpawnShadowFX(inst)
        end
    end
end

--Called from stategraph
local function LaunchProjectile(inst, targetpos)
    local x, y, z = inst.Transform:GetWorldPosition()

    local projectile = SpawnPrefab("minotaurphlem")
    projectile.Transform:SetPosition(x, y, z)

    --V2C: scale the launch speed based on distance
    --     because 15 does not reach our max range.
    local dx = targetpos.x - x
    local dz = targetpos.z - z
    local rangesq = dx * dx + dz * dz
    local maxrange = TUNING.FIRE_DETECTOR_RANGE
    local speed = easing.linear(rangesq, 15, 3, maxrange * maxrange)
    projectile.components.complexprojectile:SetHorizontalSpeed(speed)
    projectile.components.complexprojectile:SetGravity(-25)
    projectile.components.complexprojectile:Launch(targetpos, inst, inst)
end

local SLEEP_DIST_FROMHOME_SQ = 20 * 20
local SLEEP_DIST_FROMTHREAT = 40
local MAX_CHASEAWAY_DIST_SQ = 40 * 40
local MAX_TARGET_SHARES = 5
local SHARE_TARGET_DIST = 40

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function cause_obstacle_quake(inst, other)
    if not other.components.timer or not other.components.timer:TimerExists("ram_quake") then 
        TheWorld:PushEvent("ms_miniquake", { rad = 20, num = 20, duration = 2.5, target = inst })
        inst.startobstacledrop(inst)   

        if not other.components.timer then
            other:AddComponent("timer")
        end
        other.components.timer:StartTimer("ram_quake",20)
    end
end

local function breakobjects(inst,other)
    if other == inst then
        return
    end
    if other:HasTag("smashable") and other.components.health ~= nil then
        other.components.health:Kill()
    elseif other.components.workable ~= nil
        and other.components.workable:CanBeWorked()
        and other.components.workable.action ~= ACTIONS.NET then
        other.components.workable:Destroy(inst)
    elseif other.components.health ~= nil and not other.components.health:IsDead() then
        return true
    end
end

local NO_TAGS = {"FX", "NOCLICK", "DECOR", "INLIMBO", "notarget"}
local function jumpland(inst)
    local hittarget = false
    local pt = Vector3(inst.Transform:GetWorldPosition())
    local ents = TheSim:FindEntities(pt.x,pt.y,pt.z,4,nil,NO_TAGS)
    if #ents > 0 then
        for i,ent in ipairs(ents)do
            if breakobjects(inst,ent) then
               hittarget = true
            end 
        end
    end
    if not hittarget then
        inst.chargecount = 0.6
        inst:PushEvent("collision_stun",{land_stun = true})
        return false
    end
    return true
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function onothercollide(inst, other)
    --[[
	if other and other:IsValid() then
		
		if other:HasTag("smashable") and MOBFIRE "Disable" then
			--other.Physics:SetCollides(false)
			other.components.health:Kill()
		elseif other.components.workable ~= nil and other.components.workable:CanBeWorked() and MOBFIRE ~= "Disable" then
			if other:HasTag("frozen") then
				other.components.workable:SetWorkLeft(-1)
			else
			
			SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
			other.components.workable:Destroy(inst)
			end
		elseif not inst.recentlycharged[other] and other.components.health ~= nil and not other.components.health:IsDead() and inst.sg:HasStateTag("charging") then
			inst.recentlycharged[other] = true
			inst:DoTaskInTime(3, ClearRecentlyCharged, other)
			inst.components.combat:DoAttack(other)
            if not other:HasTag("structure") then

            end
            
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/rook/explo")
		end
	end]]
    if not other:IsValid() or inst.recentlycharged[other] then
        return
    else
        if other:HasTag("charge_barrier") then
            inst:PushEvent("collision_stun",{light_stun = inst.chargecount < 0.3 and true or nil})            
        end
        if other:HasTag("quake_on_charge") then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(20, ClearRecentlyCharged, other)
            cause_obstacle_quake(inst, other)
            --ShakeAllCameras(CAMERASHAKE.FULL, .3, .025, .2, inst, 30)
            ShakeAllCameras(CAMERASHAKE.VERTICAL, .7, .02, 1.1, inst, 40)
            other:PushEvent("shake")
        else
            ShakeAllCameras(CAMERASHAKE.SIDE, .5, .05, .1, inst, 40)
        end

        breakobjects(inst,other)

        if not inst.recentlycharged[other] and other.components.health ~= nil and not other.components.health:IsDead() and other.components.combat and not other.components.workable and inst.sg:HasStateTag("charging") then
			inst.recentlycharged[other] = true
			inst:DoTaskInTime(3, ClearRecentlyCharged, other)
			other.components.combat:GetAttacked(inst, other:HasTag("player") and 200 or 400)
		end

        if ( other.components.health ~= nil and not other.components.health:IsDead())
            or (other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() and (MOBFIRE ~= "Disable" and inst:HasTag("wall") or not inst:HasTag("wall"))) then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
            if other:HasTag("wall") and MOBFIRE ~= "Disable" then
                local isname = inst:GetDisplayName()
                local selfpos = inst:GetPosition()
                print("LOGGED: "..isname.." rammed a wall at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
            end
        end
    end
end

local function oncollide(inst, other)
    if not (other ~= nil and other:IsValid() and inst:IsValid())
       and TheNet:GetPVPEnabled() and other:HasTag("player")
        or Vector3(inst.Physics:GetVelocity()):LengthSq() < 42 then
        return
    end
    ShakeAllCameras(CAMERASHAKE.SIDE, .5, .05, .1, inst, 40)
    inst:DoTaskInTime(2 * FRAMES, onothercollide, other)
end

local function OnAttacked(inst, data)
    if inst.components.health:GetPercent() < PHASE1 and not inst.atphase2 then
        inst.atphase2 = true
        inst.components.ppskin_manager:LoadSkin(mob, true)
        inst:AddTag("shadow_aligned")
    end

    local attacker = data ~= nil and data.attacker or nil
    if attacker ~= nil and attacker:HasTag("chess") then
        return
    end
    inst.components.combat:SetTarget(attacker)
    inst.components.combat:ShareTarget(attacker, SHARE_TARGET_DIST, IsChess, MAX_TARGET_SHARES)

    if inst.components.health:GetPercent() < PHASE1  and not inst.tentacletask then
        inst.tentacletask = inst:DoPeriodicTask(0.2, function() checkfortentacledrop(inst) end)
    end
end

local function HealthDelta(inst)
    if inst.components.health:GetPercent() >= 0.8 then
        if inst.tentacletask then
            inst.tentacletask:Cancel()
            inst.tentacletask = nil
        end
        inst.atphase2 = nil
        inst.components.ppskin_manager:LoadSkin(mob, true)
    end
end

local function SpawnTentacle(inst, pt)
    if not pt then
        pt = Vector3(inst.Transform:GetWorldPosition())
    end
    local tent = SpawnPrefab("bigshadowtentacle")
    tent.Transform:SetPosition(pt.x,pt.y,pt.z)
    tent:PushEvent("arrive")
end

local TENTS_MUSTHAVE = {"shadow"}
local function SpawnBigBloodDrop(inst, pt)
    local mpt = Vector3(inst.Transform:GetWorldPosition())
    local radius = 4
    if not pt then
        pt = mpt
    end

    local count = TheSim:FindEntities(pt.x,pt.y,pt.z,5,TENTS_MUSTHAVE)
    for i=#count,1,-1 do
        if count[i].prefab ~= "bigshadowtentacle" then
            table.remove(count,i)
        end
    end
    if #count < 5 then
        local theta = math.random() * 2* PI
        local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))
        pt = pt+offset

        local blood = SpawnPrefab("minotaur_blood_big")
        blood.Transform:SetPosition(pt.x,pt.y,pt.z)

        blood.Transform:SetRotation(theta/DEGREES - 180)
    end
end

local function SpawnShadowFX(inst)

    local pt = Vector3(inst.Transform:GetWorldPosition())
    local radius = 1.5 + math.random()
  
    local theta = math.random() * 2* PI
    local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin(  theta ))
    --pt = pt+offset

    local fx = SpawnPrefab("minotaur_blood"..math.random(1,3))
    --local scale = 1 + (math.random()*1)
    fx.Transform:SetPosition(pt.x,pt.y,pt.z)
    --fx.Transform:SetScale(scale,scale,scale)    
    --fx.Transform:SetRotation(inst:GetAngleToPoint(pt))
    fx.Transform:SetRotation(math.random()*360)
end

local OBSTACLE_MUST_TAGS = {"charge_barrier"}

local PILLAR_MUST_TAGS = {"quake_on_charge"}
local function startobstacledrop(inst)
    if inst.spawnlocation then
        local dist = inst:GetDistanceSqToPoint(inst.spawnlocation)

        if dist < 25*25 then
            local instpt = inst.spawnlocation

            for i=1, math.random(3,4) do
                local pt = nil
                local testpt = nil
                local count = 0
                while testpt == nil and count < 8 do
                    count = count + 1
                    local theta = math.random()*2 * PI
                    local radius = math.sqrt(math.random())*20
                    testpt = Vector3(radius*math.cos(theta),0,radius*math.sin(theta))
                    testpt = testpt + instpt
                    testpt.y = 0

                    if not TheWorld.Map:IsPassableAtPoint(testpt.x,0,testpt.z) then
                        testpt = nil
                    end
                    if testpt then
                        local ents = TheSim:FindEntities(testpt.x,testpt.y,testpt.z, 4, PILLAR_MUST_TAGS)
                        if #ents > 0 then
                            testpt = nil
                        end
                    end         
                end

                if testpt then
                    inst:DoTaskInTime(0.3+(math.random()*2), function()
                        local obstacle = SpawnPrefab("ruins_cavein_obstacle")
                        obstacle.Transform:SetPosition(instpt.x,instpt.y,instpt.z)
                        obstacle.fall(obstacle,Vector3(testpt.x,testpt.y,testpt.z))
                    end)
                end
            end
        end
    end
end

--------- LANDING PHYSICS STUFF
local function CancelObstacleTask(inst)
    if inst.task ~= nil then
        inst.task:Cancel()
        inst.task = nil
        inst.ischaracterpassthrough = nil
    end
end

local function SetCurrentRadius(inst, radius)
    if inst.currentradius ~= radius then
        inst.currentradius = radius
        inst.Physics:SetCapsule(radius, 4)
    end
end

local CHARACTER_MUST_TAGS = { "character", "locomotor" }
local CHARACTER_CANT_TAGS = { "INLIMBO", "NOCLICK", "flying", "ghost" }
local function OnUpdateObstacleSize(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local mindist = math.huge
    for i, v in ipairs(TheSim:FindEntities(x, y, z, 2, CHARACTER_MUST_TAGS, CHARACTER_CANT_TAGS)) do
        if v.entity:IsVisible() then
            local d = v:GetDistanceSqToPoint(x, y, z)
            d = d > 0 and (v.Physics ~= nil and math.sqrt(d) - v.Physics:GetRadius() or math.sqrt(d)) or 0
            if d < mindist then
                if d <= 0 then
                    mindist = 0
                    break
                end
                mindist = d
            end
        end
    end
    local radius = math.clamp(mindist, 0, inst.maxradius)
    if radius > 0 then
        SetCurrentRadius(inst, radius)
        if inst.ischaracterpassthrough then
            inst.ischaracterpassthrough = nil
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
        end
        if radius >= inst.maxradius then
            CancelObstacleTask(inst)
        end
    end
end

local function OnChangeToObstacle(inst)
    inst.Physics:SetMass(100)
    inst.Physics:SetCollisionGroup(COLLISION.GIANTS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
    inst.Physics:CollidesWith(COLLISION.GIANTS)
    
    inst.ischaracterpassthrough = true
    inst.task = inst:DoPeriodicTask(.5, OnUpdateObstacleSize)
    OnUpdateObstacleSize(inst)
    inst.Physics:Teleport(inst.Transform:GetWorldPosition())
end

local function checkstunend(inst, data)
    if data ~= nil then
        if data.name == "endstun" then
            inst:RestartBrain()
            if inst.AnimState:IsCurrentAnimation("stun_jump_pre") or
                inst.AnimState:IsCurrentAnimation("stun_pre") or
                inst.AnimState:IsCurrentAnimation("stun_loop") or
                inst.AnimState:IsCurrentAnimation("stun_hit") then
                inst.sg:GoToState("stun_pst")
            end
        end
    end
end
--==============================================
--				Custom Common Functions
--==============================================	

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================
local function SetSkinDefault(inst, data)
    if data then
		inst.AnimState:SetBuild(inst.atphase2 and (data.build2 or mob.build2) or data.build)
	else
		inst.AnimState:SetBuild(inst.atphase2 and mob.build2 or mob.build)
	end	
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("monster")
    inst:AddTag("minotaur")
    inst:AddTag("epic")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local function OnDeath(inst)
    inst.atphase2 = nil
    if inst.tentacletask then
        inst.tentacletask:Cancel()
        inst.tentacletask = nil
    end
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
    inst.altattack = true
	inst.shouldwalk = true
	inst.mobplayer = true
	inst.isshiny = 0
    inst.chargecount = 0

    inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	inst.recentlycharged = {}

	inst.maxradius = 1.75

	inst.SpawnShadowFX = SpawnShadowFX
    inst.SpawnBigBloodDrop = SpawnBigBloodDrop
    inst.startobstacledrop = startobstacledrop
    inst.jumpland = jumpland
    inst.OnChangeToObstacle = OnChangeToObstacle

    inst.PHASE1 = PHASE1
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, "swap_fire")
    MakeLargeFreezableCharacter(inst, "innerds")
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride

	inst:AddComponent("timer")

    inst:AddComponent("groundpounder")
    inst.components.groundpounder.numRings = 2
	----------------------------------
	--Eater--
    inst.components.eater:SetCanEatHorrible()
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
    inst.physicsradius = 2.2

    MakeGiantCharacterPhysics(inst, 1000, inst.physicsradius)
    inst.Physics:SetCapsule(1.75, 4) -- update inst.maxradius if you change this
	inst:SetPhysicsRadiusOverride(2.2)
	inst.DynamicShadow:SetSize(5, 3)
	inst.Physics:SetCollisionCallback(oncollide)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:DoTaskInTime(0, function() OnAttacked(inst) end)
	inst:ListenForEvent("attacked", OnAttacked)
    inst:ListenForEvent("healthdelta", HealthDelta)
    inst:ListenForEvent("death", OnDeath)
    inst:ListenForEvent("timerdone", checkstunend)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)