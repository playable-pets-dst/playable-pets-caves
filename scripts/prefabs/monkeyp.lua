local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "monkeyp"

local assets = 
{
	Asset("ANIM", "anim/kiki_shiny_build_01.zip"),
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {"1"}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 100,
	hungerrate = 0.15, --I think .25 is defualt.
	sanity = 175,
	runspeed = TUNING.MONKEY_MOVE_SPEED,
	walkspeed = TUNING.MONKEY_MOVE_SPEED,
	damage = 32*2,
	attackperiod = 0,
	range = 2,
	hit_range = 3,
	bank = "kiki",
	build = "kiki_basic",
	scale = 1,
	stategraph = "SGmonkeyp",
	minimap = "monkeyp.tex",
}

--Loot that drops when you die, duh.
SetSharedLootTable('monkeyp',
-----Prefab---------------------Chance------------
{
    {'smallmeat',			1.00},
})

local FORGE_STATS = PPC_FORGE.MONKEY
--==============================================
--					Mob Functions
--==============================================
local function DoFx(inst)
    inst.SoundEmitter:PlaySound("dontstarve/common/ghost_spawn")

    local x, y, z = inst.Transform:GetWorldPosition()
    local fx = SpawnPrefab("statue_transition_2")
    if fx ~= nil then
        fx.Transform:SetPosition(x, y, z)
        fx.Transform:SetScale(.8, .8, .8)
    end
    fx = SpawnPrefab("statue_transition")
    if fx ~= nil then
        fx.Transform:SetPosition(x, y, z)
        fx.Transform:SetScale(.8, .8, .8)
    end
end

local function SetNormalMonkey(inst)
	if not inst:HasTag("playerghost") then
    inst:RemoveTag("nightmare")
    inst.AnimState:SetBuild("kiki_basic")
    inst.AnimState:SetMultColour(1, 1, 1, 1)
    inst.soundtype = ""
	end   
end

local function SetNightmareMonkey(inst)
	if not inst:HasTag("playerghost") then
		inst:AddTag("nightmare")
		inst.AnimState:SetMultColour(1, 1, 1, .6)
		inst.AnimState:SetBuild("kiki_nightmare_skin")
		inst.soundtype = "_nightmare"
		if inst.task ~= nil then
			inst.task:Cancel()
			inst.task = nil
		end
	end
end

local function TestNightmareArea(inst, area)
    if (TheWorld.state.isnightmarewild or TheWorld.state.isnightmaredawn)
        and inst.components.areaaware:CurrentlyInTag("Nightmare")
        and not inst:HasTag("nightmare") then

        DoFx(inst)
        SetNightmareMonkey(inst)
    elseif (not TheWorld.state.isnightmarewild and not TheWorld.state.isnightmaredawn)
        and inst:HasTag("nightmare") then
        DoFx(inst)
        SetNormalMonkey(inst)
    end
end

local function TestNightmarePhase(inst, phase)
    if (phase == "wild" or phase == "dawn")
        and inst.components.areaaware:CurrentlyInTag("Nightmare")
        and not inst:HasTag("nightmare") then

        DoFx(inst)
        SetNightmareMonkey(inst)
    elseif (phase ~= "wild" and phase ~= "dawn")
        and inst:HasTag("nightmare") then
        DoFx(inst)
        SetNormalMonkey(inst)
    end
end
--==============================================
--				Custom Common Functions
--==============================================	
local function IsPoop(item)
    return item.prefab == "poop"
end

local function OnEat(inst, food)
    --Eating food might cause pooping!
    if inst.components.inventory ~= nil then
        local maxpoop = 20
		local pooper = math.random()
        local poopstack = inst.components.inventory:FindItem(IsPoop)
        if poopstack == nil or poopstack.components.stackable.stacksize < maxpoop then
			if math.random() < 0.33 then
            inst.components.inventory:GiveItem(SpawnPrefab("poop"))
			end
        end
    end
end

local function FireProjectile(inst, target)
    if target then
		local pos = inst:GetPosition()
		local proj = SpawnPrefab("monkeyprojectile")
		proj.Transform:SetPosition(pos.x, 0, pos.z)
		inst:DoTaskInTime(0, function(inst) proj.components.projectile.owner = inst end)
		proj.components.projectile.owner = inst
		proj.components.projectile:Throw(inst, target)
    end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("monkey")
	inst:AddTag("animal")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	inst.soundtype = "" --must be set before SG, unless you want to alter the idle state to delay the sound by a frame or two.
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, nil, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.mobplayer = true
	inst._ismonster = true
	inst.isshiny = 0
	inst.poopmode = false
	
	inst.FireProjectile = FireProjectile
	
	local body_symbol = "kiki_lowerbody"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	inst:AddComponent("areaaware")
	
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }, { FOODTYPE.SEEDS, FOODTYPE.GENERIC, FOODTYPE.VEGGIE }) 
	inst.components.eater:SetOnEatFn(OnEat)
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .25)
    inst.DynamicShadow:SetSize(2, 1.25)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)