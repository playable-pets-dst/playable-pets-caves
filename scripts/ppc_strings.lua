STRINGS.RECIPE_DESC.RABBITHOUSE_PLAYER = "Classic House of Carrots."
STRINGS.RECIPE_DESC.SPIDERHOLE_P = "For that hardcore spider."
STRINGS.RECIPE_DESC.BATCAVE_P = "Bat Man not included."
STRINGS.RECIPE_DESC.MONKEYBARREL_P = "For the leader of your DS crew"
STRINGS.RECIPE_DESC.SLURTLEMOUND_P = "Hard and slimy... don't look at me like that."

STRINGS.CHARACTERS.BUNNYP = require "speech_bunnyp"
-- The character select screen lines
----------------------------------------------------------
STRINGS.CHARACTER_TITLES.shadow2player = "Terrorbeak"
STRINGS.CHARACTER_NAMES.shadow2player = "TERRORBEAK"
STRINGS.CHARACTER_DESCRIPTIONS.shadow2player = "*One of Them\n*Transforms when reaching the ocean\n*Is always watching..."
STRINGS.CHARACTER_QUOTES.shadow2player = "Them..."

STRINGS.CHARACTER_TITLES.shadowplayer = "Crawling Horror"
STRINGS.CHARACTER_NAMES.shadowplayer = "CRAWLING HORROR"
STRINGS.CHARACTER_DESCRIPTIONS.shadowplayer = "*One of Them\n*Is always watching..."
STRINGS.CHARACTER_QUOTES.shadowplayer = "Them..."

STRINGS.CHARACTER_TITLES.wormp = "Depths Worm"
STRINGS.CHARACTER_NAMES.wormp = "DEPTHS WORM"
STRINGS.CHARACTER_DESCRIPTIONS.wormp = "*Digs underground\n*Has a lure"
STRINGS.CHARACTER_QUOTES.wormp = "Worms are never a good thing."

STRINGS.CHARACTER_TITLES.batp = "Batilisk"
STRINGS.CHARACTER_NAMES.batp = "BATILISK"
STRINGS.CHARACTER_DESCRIPTIONS.batp = "*Is flying\n*Poops\n*Not very strong..."
STRINGS.CHARACTER_QUOTES.batp = "Flying rats."

STRINGS.CHARACTER_TITLES.cspiderp = "Spitter"
STRINGS.CHARACTER_NAMES.cspiderp = "SPITTER"
STRINGS.CHARACTER_DESCRIPTIONS.cspiderp = "*Is a Spider\n*Can spit at a range\n*Not very good in melee combat"
STRINGS.CHARACTER_QUOTES.cspiderp = "Spitting spitter spit"

STRINGS.CHARACTER_TITLES.cspider2p = "Hider"
STRINGS.CHARACTER_NAMES.cspider2p = "HIDER"
STRINGS.CHARACTER_DESCRIPTIONS.cspider2p = "*Is a Spider\n*Can hide in their protective shell\n*Is bulky for a spider"
STRINGS.CHARACTER_QUOTES.cspider2p = "Its as tough as it is edgy."

STRINGS.CHARACTER_TITLES.monkeyp = "Splumonkey"
STRINGS.CHARACTER_NAMES.monkeyp = "SPLUMONKEY"
STRINGS.CHARACTER_DESCRIPTIONS.monkeyp = "*Is a Monkey\n*Can throw poop\n*Is a vegetarian"
STRINGS.CHARACTER_QUOTES.monkeyp = "Where are my monkey barrels Klei!?"

STRINGS.CHARACTER_TITLES.slurperp = "Slurper"
STRINGS.CHARACTER_NAMES.slurperp = "SLURPER"
STRINGS.CHARACTER_DESCRIPTIONS.slurperp = "*Is a glutton\n*Gives light \n*Can steal hunger on hit"
STRINGS.CHARACTER_QUOTES.slurperp = "Careful Gordon, she might try to couple with your head."

STRINGS.CHARACTER_TITLES.slurtlep = "Slurtle"
STRINGS.CHARACTER_NAMES.slurtlep = "SLURTLE"
STRINGS.CHARACTER_DESCRIPTIONS.slurtlep = "*Is very slow\n*Can hide in their protective shell\n*Is prone to exploding\n*Can only eat rocks"
STRINGS.CHARACTER_QUOTES.slurtlep = "He will eat all the rocks... eventually."

STRINGS.CHARACTER_TITLES.bunnyp = "Bunnyman"
STRINGS.CHARACTER_NAMES.bunnyp = "BUNNYMAN"
STRINGS.CHARACTER_DESCRIPTIONS.bunnyp = "*Is a vegetarian\n*Can see in the dark \n*Transforms when low on sanity"
STRINGS.CHARACTER_QUOTES.bunnyp = "MEAT IS MURDER!!!1!"

STRINGS.CHARACTER_TITLES.rocklobsterp = "Rock Lobster"
STRINGS.CHARACTER_NAMES.rocklobsterp = "ROCKLOBSTER"
STRINGS.CHARACTER_DESCRIPTIONS.rocklobsterp = "*Super bulky and slow \n*Can protect themselves\n*Can only eat rocks"
STRINGS.CHARACTER_QUOTES.rocklobsterp = "Reminds me of that one song..."

STRINGS.CHARACTER_TITLES.guardianp = "Ancient Guardian"
STRINGS.CHARACTER_NAMES.guardianp = "GUARDIAN"
STRINGS.CHARACTER_DESCRIPTIONS.guardianp = "*Is a Giant\n*Can ram"
STRINGS.CHARACTER_QUOTES.guardianp = "The REAL and ORIGINAL Guardian."

STRINGS.CHARACTER_TITLES.hutchp = "Hutch"
STRINGS.CHARACTER_NAMES.hutchp = "HUTCH"
STRINGS.CHARACTER_DESCRIPTIONS.hutchp = "*Is adorable \n*Can transform when holding certain items\n*Has a light"
STRINGS.CHARACTER_QUOTES.hutchp = "Chester but better?"

-- The character's name as appears in-game 
STRINGS.NAMES.MONKEYBARREL_P = "Splumonkey Pod"
STRINGS.NAMES.MONKEYHOUSE = "Splumonkey Pod"

STRINGS.NAMES.SPIDERHOLE_P = "Spider Mound"
STRINGS.NAMES.CSPIDERHOME = "Spider House"

STRINGS.NAMES.SLURTLEMOUND_P = "Slurtle Mound"
STRINGS.NAMES.SLURTLEHOME = "Slurtle House"

STRINGS.NAMES.BATCAVE_P = "Batilisk Cave"
STRINGS.NAMES.BATHOME = "Bat House"

STRINGS.NAMES.RABBITHOUSE_PLAYER = "Rabbit Hutch"
STRINGS.NAMES.CMONSTER_WPN = "Myterious Power"
-- The default responses of examining the prefab

STRINGS.CHARACTERS.GENERIC.DESCRIBE.RABBITHOUSE_PLAYER = 
{
	GENERIC = "Smells like carrots.",
	BURNT = "Smells like burnt carrots... and rabbits.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BATCAVE_P = 
{
	GENERIC = "Home of Batman.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.SPIDERHOLE_P = 
{
	GENERIC = "Doesn't look very comfy.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.CMONSTER_WPN = 
{
	GENERIC = "I shouldn't have this...",
}

return STRINGS