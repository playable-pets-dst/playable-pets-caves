local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "slurperp"

local assets = 
{
	Asset("ANIM", "anim/slurper_shiny_build_01.zip"),
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 175,
	hunger = 300,
	hungerrate = 0.25, --I think .25 is defualt.
	sanity = 125,
	runspeed = TUNING.SLURPER_WALKSPEED,
	walkspeed = TUNING.SLURPER_WALKSPEED,
	damage = 30*2,
	range = TUNING.SLURPER_ATTACK_DIST,
	hit_range = 3,
	bank = "slurper",
	build = "slurper_basic",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGslurperp",
	minimap = "slurperp.tex",
}

--Loot that drops when you die, duh.
SetSharedLootTable('slurperp',
-----Prefab---------------------Chance------------
{
    {'slurperpelt',			0.50},
	{'lightbulb',			1.00},
	{'lightbulb',			1.00},
})

local FORGE_STATS = PPC_FORGE.SLURPER
--==============================================
--					Mob Functions
--==============================================
local light_params =
{
    low =
    {
        radius = 1,
        intensity = .5,
        falloff = .7,
    },
    high =
    {
        radius = 3,
        intensity = .8,
        falloff = .4,
    },
}

local MAX_LIGHT_FRAME = math.floor(2 / FRAMES + .5)

local function OnUpdateLight(inst, dframes)
    local done
    if inst._lightlevel:value() then
        local frame = inst._lightframe:value() + dframes
        done = frame >= MAX_LIGHT_FRAME
        inst._lightframe:set_local(done and MAX_LIGHT_FRAME or frame)
    else
        local frame = inst._lightframe:value() - dframes
        done = frame <= 0
        inst._lightframe:set_local(done and 0 or frame)
    end

    local k = inst._lightframe:value() / MAX_LIGHT_FRAME
    local k1 = 1 - k
    inst.Light:SetRadius(light_params.high.radius * k + light_params.low.radius * k1)
    inst.Light:SetIntensity(light_params.high.intensity * k + light_params.low.intensity * k1)
    inst.Light:SetFalloff(light_params.high.falloff * k + light_params.low.falloff * k1)

    if done then
        inst._lighttask:Cancel()
        inst._lighttask = nil
    end
end

local function OnLightDirty(inst)
    if inst._lighttask == nil then
        inst._lighttask = inst:DoPeriodicTask(FRAMES, OnUpdateLight, nil, 1)
    end
    OnUpdateLight(inst, 0)
end

--Hat

local function CanHatTarget(inst, target)
    if target == nil or
        target.components.inventory == nil or
        not (target.components.inventory.isopen or
            target:HasTag("pig") or
            target:HasTag("manrabbit") or
            (inst._loading and target:HasTag("player"))) then
        --NOTE: open inventory implies player, so we can skip "player" tag check
        --      closed inventory on player means they shouldn't be able to equip
        --      EXCEPT during load, because player inventory opens after 1 frame
        return false
    end
    local hat = target.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    return hat == nil or hat.prefab ~= inst.prefab
end

local function OnEquip(inst, owner)
    --Start feeding!

    if not CanHatTarget(inst, owner) then
        owner.components.inventory:Unequip(EQUIPSLOTS.HEAD)
        return
    end

    inst.Light:Enable(true)
    

    inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/attach")

    owner.AnimState:OverrideSymbol("swap_hat", "hat_slurper", "swap_hat")
    owner.AnimState:Show("HAT")
    owner.AnimState:Show("HAT_HAIR")
    owner.AnimState:Hide("HAIR_NOHAT")
    owner.AnimState:Hide("HAIR")

    if owner:HasTag("player") then
        owner.AnimState:Hide("HEAD")
        owner.AnimState:Show("HEAD_HAT")

        inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/headslurp", "slurp_loop")
    else
        inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/headslurp_creatures", "slurp_loop")
    end

    inst.shouldburp = true
    inst.cansleep = false

    inst.onattach(owner)

    if inst.task ~= nil then
        inst.task:Cancel()
    end
    inst.task = inst:DoPeriodicTask(2, slurphunger, nil, owner)
end

local function OnUnequip(inst, owner)
    inst.Light:Enable(true) 
    

    inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/dettach")

    owner.AnimState:ClearOverrideSymbol("swap_hat")
    owner.AnimState:Hide("HAT")
    owner.AnimState:Hide("HAT_HAIR")
    owner.AnimState:Show("HAIR_NOHAT")
    owner.AnimState:Show("HAIR")

    if owner:HasTag("player") then
        owner.AnimState:Show("HEAD")
        owner.AnimState:Hide("HEAD_HAT")
    end
    inst.SoundEmitter:KillSound("slurp_loop")

    inst.ondetach(owner)

    if inst.task ~= nil then
        inst.task:Cancel()
    end
    inst.task = inst:DoTaskInTime(10, setcansleep)
end
--==============================================
--				Custom Common Functions
--==============================================	
local function OnHitOther(inst, other) --TODO temporary ability until I tackle the slurper hat issues
    if other and other.components.hunger then
		other.components.hunger:DoDelta(-10)
		inst.components.hunger:DoDelta(10)
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:RemoveComponent("shedder")
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("cavedweller")
    inst:AddTag("mufflehat")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	--inst.taunt2 = true --Will be used to manually unequip self from target
	inst.mobplayer = true
	inst.isshiny = 0
	inst.getskins = getskins
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst:AddComponent("shedder")
	inst.components.shedder.shedItemPrefab = "lightbulb"
	inst.components.shedder.shedHeight = 0.1
	inst.components.shedder:StartShedding(1200) --Note: 480 is 1 day. 480 x n = n amount of days.
	----------------------------------
	--Eater--
	inst.components.eater:SetAbsorptionModifiers(1,1,1) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .5)
    inst.DynamicShadow:SetSize(2, 1.25)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst.Light:Enable(true)
  	inst.Light:SetRadius(light_params.low.radius)
    inst.Light:SetIntensity(light_params.low.intensity)
    inst.Light:SetFalloff(light_params.low.falloff)
    inst.Light:SetColour(237/255, 237/255, 209/255)
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) 
			inst.Light:Enable(true)
			inst.Light:SetRadius(light_params.low.radius)
			inst.Light:SetIntensity(light_params.low.intensity)
			inst.Light:SetFalloff(light_params.low.falloff)
			inst.Light:SetColour(237/255, 237/255, 209/255)
		end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)