local DEFAULT_SCALE = 0.20
local Puppets = {
    batp = {bank = "bat", build = "bat_basic", anim = "fly_loop", scale = 0.15, shinybuild = "bat", y_offset = -20 },
	wormp = {bank = "worm", build = "worm", anim = "taunt", scale = 0.15, shinybuild = "worm" },
	cspiderp = {bank = "spider_spitter", build = "ds_spider2_caves", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "spitter"},
	cspider2p = {bank = "spider_hider", build = "DS_spider_caves", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "hider"},
	monkeyp = {bank = "kiki", build = "kiki_basic", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "kiki", sixfaced = true},
	hutchp = {bank = "hutch", build = "hutch_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "hutch"},
	slurperp = {bank = "slurper", build = "slurper_basic", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "slurper"},
	slurtlep = {bank = "slurtle", build = "slurtle", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "slurtle"},
	bunnyp = {bank = "manrabbit", build = "manrabbit_build", anim = "idle_loop", scale = 0.20, shinybuild = "manrabbit"},
	guardianp = {bank = "rook", build = "rook_rhino", anim = "idle", scale = 0.10, shinybuild = "guardian"},
	rocklobsterp = {bank = "rocky", build = "rocky", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "rocky"},
	shadowplayer = {bank = "shadowcreature1", build = "shadow_insanity1_basic", anim = "idle_loop", scale = 0.15, shinybuild = "crawlinghorror", uiscale = 0.125},
	shadow2player = {bank = "shadowcreature2", build = "shadow_insanity2_basic", anim = "idle_loop", scale = 0.15, shinybuild = "terrorbeak", uiscale = 0.11},
}

return Puppets