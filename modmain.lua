
------------------------------------------------------------------
-- Variables
------------------------------------------------------------------
local require = GLOBAL.require
local Ingredient = GLOBAL.Ingredient
-- Add it to the tech trees


local PlayablePets = GLOBAL.PlayablePets
local SETTING = GLOBAL.PP_SETTINGS
local MOBTYPE = GLOBAL.PP_MOBTYPES

local STRINGS = require("ppc_strings")

local TUNING = require("ppc_tuning")

GLOBAL.PPC_FORGE = require("ppc_forge")
------------------------------------------------------------------
-- Configuration Data
------------------------------------------------------------------

PlayablePets.Init(env.modname)
------------------------------------------------------------------
-- Prefabs
------------------------------------------------------------------

PrefabFiles = {
	-------Houses--------
	"monkeybarrel_p",
	"spiderhole_p",
	"batcave_p",
	"slurtlemound_p",
	----------
	"cmonster_wpn",
	"monkeyhouse",
	"cspiderhome",
	"slurtlehome",
	"rabbithouse_player",
	"bathome",
}

-- Centralizes a number of related functions about playable mobs to one convenient table
-- Name is the prefab name of the mob
-- Fancyname is the name of the mob, as seen in-game
-- Gender is fairly self-explanatory
-- Skins is a list of any custom mob skins that may be available, using the Modded Skins API by Fidoop
-- Skins are not required for mobs to function, but they will display a custom portrait when the mob is examined by a player
GLOBAL.PPC_MobCharacters = {
	shadow2player       	 = { fancyname = "Terrorbeak",       gender = "MALE",   mobtype = {}, skins = {}},
	wormp       	 = { fancyname = "Depth Worm",       gender = "FEMALE",   mobtype = {}, skins = {}},
	batp       	 = { fancyname = "Batilisk",       gender = "MALE",   mobtype = {}, skins = {}},
	cspiderp       	 = { fancyname = "Spitter",       gender = "MALE",   mobtype = {}, skins = {}}, --forge = true },
	cspider2p        = { fancyname = "Hider",           gender = "MALE",   mobtype = {}, skins = {}}, --forge = true },
	hutchp        = { fancyname = "Hutch",           gender = "FEMALE",   mobtype = {}, skins = {}}, --forge = true },
	monkeyp        = { fancyname = "Splumonkey",           gender = "MALE",   mobtype = {}, skins = {}}, --forge = true },
	slurperp        = { fancyname = "Slurper",           gender = "FEMALE",   mobtype = {}, skins = {}}, --forge = true },
	slurtlep        = { fancyname = "Slurtle",           gender = "MALE",   mobtype = {}, skins = {}}, --forge = true },
	bunnyp        = { fancyname = "Bunnyman",           gender = "MALE",   mobtype = {}, skins = {}}, --forge = true },
	rocklobsterp        = { fancyname = "Rock Lobster",           gender = "FEMALE",   mobtype = {}, skins = {}}, --forge = true},
	guardianp        = { fancyname = "Guardian",           gender = "MALE",   mobtype = {}, skins = {}}, --forge = true},
	shadowplayer        = { fancyname = "Crawling Horror",           gender = "MALE",   mobtype = {}, skins = {}}, --forge = true},
}



-- Necessary to ensure a specific order when adding mobs to the character select screen. This table is iterated and used to index the one above
PPC_Character_Order = {
	"batp",
	"cspiderp",
	"cspider2p",
	"slurperp",
	"slurtlep",
	"wormp",
	"hutchp",
	"monkeyp",
	"bunnyp",
	"rocklobsterp",
	"shadowplayer",
	"shadow2player",
	"guardianp",
}
------------------------------------------------------------------
-- Assets
------------------------------------------------------------------

Assets = {
	--------------------------------------------------------------
	Asset("ANIM", "anim/ghost_monster_build.zip"), 
	--------------------------------------------------------------
	Asset("IMAGE", "images/inventoryimages/cmonster_wpn.tex"),
	Asset("ATLAS", "images/inventoryimages/cmonster_wpn.xml"),
	-----------------------------------------------------------------
	Asset("IMAGE", "images/inventoryimages/monkeyhouse.tex"),
	Asset("ATLAS", "images/inventoryimages/monkeyhouse.xml"),
	Asset("IMAGE", "images/inventoryimages/bathome.tex"),
	Asset("ATLAS", "images/inventoryimages/cspiderhome.xml"),
	Asset("IMAGE", "images/inventoryimages/slurtlehome.tex"),
	Asset("ATLAS", "images/inventoryimages/slurtlehome.xml"),
	Asset( "IMAGE", "images/inventoryimages/rabbithouse_player.tex" ),
    Asset( "ATLAS", "images/inventoryimages/rabbithouse_player.xml" ),
}

------------------------------------------------------------------
-- Custom Recipes
------------------------------------------------------------------

local Ingredient = GLOBAL.Ingredient
local bunnyrecipe = AddRecipe("rabbithouse_player", {Ingredient("boards", 4), Ingredient("carrot", 10),Ingredient("foliage", 10)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_TWO, "rabbithouse_placer", nil, nil, 1,"bunnyplayer", "images/inventoryimages/rabbithouse_player.xml", "rabbithouse_player.tex" )
bunnyrecipe.sortkey = -11.0
--Homes--
local MobHouseMethod = PlayablePets.GetModConfigData("MobHouseMethod")
if MobHouseMethod == SETTING.HOUSE_ON_CRAFT or MobHouseMethod == SETTING.HOUSE_BOTH then
	local spiderholerecipe = AddRecipe("spiderhole_p", {Ingredient("rocks", 10), Ingredient("silk", 8)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "spiderhole_p_placer", nil, nil, 1, nil, "images/inventoryimages/cspiderhome.xml", "cspiderhome.tex" )
	local batcaverecipe = AddRecipe("batcave_p", {Ingredient("rocks", 8), Ingredient("guano", 3), Ingredient("batwing", 3)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "batcave_p_placer", nil, nil, 1, nil, "images/inventoryimages/bathome.xml", "bathome.tex" )
	local monkeypodrecipe = AddRecipe("monkeybarrel_p", {Ingredient("boards", 4), Ingredient("cutgrass", 6)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "monkeyhouse_placer", nil, nil, 1, nil, "images/inventoryimages/monkeyhouse.xml", "monkeyhouse.tex" )
	local slurtlemoundrecipe = AddRecipe("slurtlemound_p", {Ingredient("slurtleslime", 7), Ingredient("slurtle_shellpieces", 5)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "slurtlemound_p_placer", nil, nil, 1, nil, "images/inventoryimages/slurtlehome.xml", "slurtlehome.tex" )
end
STRINGS.RECIPE_DESC.RABBITHOUSE_PLAYER = "Classic House of Carrots."
STRINGS.RECIPE_DESC.SPIDERHOLE_P = "For that hardcore spider."
STRINGS.RECIPE_DESC.BATCAVE_P = "Bat Man not included."
STRINGS.RECIPE_DESC.MONKEYBARREL_P = "For the leader of your DS crew"
STRINGS.RECIPE_DESC.SLURTLEMOUND_P = "Hard and slimy... don't look at me like that."

STRINGS.CHARACTERS.BUNNYP = require "speech_bunnyp"
------------------------------------------------------------------
-- Component Overrides
------------------------------------------------------------------




------------------------------------------------------------------
-- PostInits
------------------------------------------------------------------	
--Monkey Barrel's slots--
_G = GLOBAL
local params={} 
params.monkeybarrel_p =
{
    widget =
    {
        slotpos = {},
        --animbank = "ui_chester_shadow_3x4",
        --animbuild = "ui_chester_shadow_3x4",
        pos = _G.Vector3(350, 350, 0),
        side_align_tip = 80, --160
    },
    type = "chest",
}
-- y and x make the slots. 0 counts as a row.
for y = 0, 5 do --6.5, -0.5, -1
    for x = 0, 5 do
        table.insert(params.monkeybarrel_p.widget.slotpos, _G.Vector3(75 * x - 75 * 12 + 75, 75 * y - 75 * 6 + 75, 0))
    end
end

local containers = _G.require "containers"
containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, params.monkeybarrel_p.widget.slotpos ~= nil and #params.monkeybarrel_p.widget.slotpos or 0)
local old_widgetsetup = containers.widgetsetup
function containers.widgetsetup(container, prefab, data)
        local pref = prefab or container.inst.prefab
        if pref == "monkeybarrel_p" then
                local t = params[pref]
                if t ~= nil then
                        for k, v in pairs(t) do
                                container[k] = v
                        end
                        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
                end
        else
                return old_widgetsetup(container, prefab)
    end
end
-------------------------------------------------------
--Wardrobe stuff--

--Skin Puppet Stuff

local MobPuppets = require("ppc_puppets")
local MobSkins = require("ppc_skins")
PlayablePets.RegisterPuppetsAndSkins(PPC_Character_Order, MobPuppets, MobSkins)
------------------------------------------------------------------
-- Commands
------------------------------------------------------------------

------------------------------------------------------------------
-- Asset Population
------------------------------------------------------------------

local assetPaths = { "bigportraits/", "images/map_icons/", "images/avatars/avatar_", "images/avatars/avatar_ghost_" }
local assetTypes = { {"IMAGE", "tex"}, {"ATLAS", "xml"} }

-- Iterate through the player mob table and do the following:
-- 1. Populate the PrefabFiles table with the mob prefab names and their skin prefabs (if applicable)
-- 2. Add an atlas and image for the mob's following assets:
-- 2.1 Character select screen portraits
-- 2.2 Character map icons
-- 2.3 ??? FIXME
-- 2.4 ??? FIXME
--for prefab, mob in pairs(GLOBAL.PPC_MobCharacters) do
for _, prefab in ipairs(PPC_Character_Order) do
	local mob = GLOBAL.PPC_MobCharacters[prefab]
	if PlayablePets.MobEnabled(mob, env.modname) then
		table.insert(PrefabFiles, prefab)
	end
	
	-- Add custom skin prefabs, if available
	-- Example: "dragonplayer_formal"
	for _, skin in ipairs(mob.skins) do
			table.insert(PrefabFiles, prefab.."_"..skin)
	end
	
	for _, path in ipairs(assetPaths) do
		for _, assetType in ipairs(assetTypes) do
			--print("Adding asset: "..assetType[1], path..prefab.."."..assetType[2])
			table.insert( Assets, Asset( assetType[1], path..prefab.."."..assetType[2] ) )
		end
	end
end

------------------------------------------------------------------
-- Mob Character Instantiation
------------------------------------------------------------------
-- Adds a mod character based on an individual mob
-- prefab is the prefab name (e.g. clockwork1player)
-- mob.fancyname is the mob's ingame name (e.g. Knight)
-- mob.gender is fairly self-explanatory
--for prefab, mob in pairs(GLOBAL.PPC_MobCharacters) do
for _, prefab in ipairs(PPC_Character_Order) do
	local mob = GLOBAL.PPC_MobCharacters[prefab]
	PlayablePets.SetGlobalData(prefab)
	--CurrentRelease.PrintID()
	if PlayablePets.MobEnabled(mob, env.modname) then
		AddMinimapAtlas("images/map_icons/"..prefab..".xml")
		AddModCharacter(prefab, mob.gender)		
		if GLOBAL.SKIN_RARITY_COLORS.ModMade ~= nil then
			GLOBAL.MakeModCharacterSkinnable(prefab)
		end
	end
end