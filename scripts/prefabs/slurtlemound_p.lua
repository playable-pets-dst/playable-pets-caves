require "prefabutil"

local assets =
{
    
}

local prefabs =
{
    "collapse_big",
}

SetSharedLootTable('slurtlemound_p',
{
    {"slurtleslime", 1.0},
    {"slurtleslime", 1.0},
    {"slurtleslime", 1.0},
    {"slurtle_shellpieces", 1.0},
})

local loot =
{
	"slurtleslime",
	"slurtleslime",
	"slurtleslime",
	"slurtle_shellpieces",

}



local function PlayLegBurstSound(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/legburst")
end


local function onhammered(inst, worker)
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
	inst.AnimState:PlayAnimation("break")
    inst.AnimState:PushAnimation("idle_broken")
    inst:RemoveComponent("childspawner")
    inst.components.lootdropper:DropLoot()
   -- local fx = SpawnPrefab("collapse_big")
    --fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    --fx:SetMaterial("wood")
    inst:Remove()
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
        if inst.components.childspawner ~= nil then
            inst.components.childspawner:ReleaseAllChildren(worker)
        end
        inst.AnimState:PlayAnimation("hit")
        inst.AnimState:PushAnimation("idle", true)
    end
end

local function onbuilt(inst)
    --inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle", true)
end



local function OnEntityWake(inst)
    --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/mound_LP", "loop")
end

local function OnEntitySleep(inst)
    --inst.SoundEmitter:KillSound("loop")
end

local function onsave(inst, data)
    if inst:HasTag("burnt") or (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
        data.burnt = true
    end
end

local function onload(inst, data)
    if data ~= nil and data.burnt then
        inst.components.burnable.onburnt(inst)
    end
end

local function onignite(inst)
    inst.components.sleepingbag:DoWakeUp()
	inst.AnimState:PlayAnimation("shake", true)
    inst.SoundEmitter:PlaySound("dontstarve/common/blackpowder_fuse_LP", "hiss")
end

local function OnExtinguishFn(inst)
    inst.SoundEmitter:KillSound("hiss")
end

local function OnExplodeFn(inst)
    inst.SoundEmitter:KillSound("hiss")
    SpawnPrefab("explode_small_slurtlehole").Transform:SetPosition(inst.Transform:GetWorldPosition())
end

local function onburntup(inst)
    --inst.AnimState:PlayAnimation("burnt_rundown")
end

local function wakeuptest(inst, phase)
    --if phase ~= inst.sleep_phase then
        --inst.components.sleepingbag:DoWakeUp()
    --end
end

local function onwake(inst, sleeper, nostatechange)
    if inst.sleeptask ~= nil then
        inst.sleeptask:Cancel()
        inst.sleeptask = nil
    end
	
	inst.SoundEmitter:KillSound("loop")
	
	inst.AnimState:PlayAnimation("idle", true)
	
    inst:StopWatchingWorldState("phase", wakeuptest)
    sleeper:RemoveEventCallback("onignite", onignite, inst)

    if not nostatechange then
        if sleeper.sg:HasStateTag("house_sleep") then
            sleeper.sg.statemem.iswaking = true
        end
		--inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
        sleeper.sg:GoToState("taunt")
    end

    if inst.sleep_anim ~= nil then
        inst.AnimState:PushAnimation("idle", true)
    end

    --inst.components.finiteuses:Use()
end

local function OnKilled(inst)
    inst.AnimState:PlayAnimation("break")
    inst.AnimState:PushAnimation("idle_broken")
    inst.components.lootdropper:DropLoot(inst:GetPosition())
end

local function onsleeptick(inst, sleeper)
    local isstarving = sleeper.components.beaverness ~= nil and sleeper.components.beaverness:IsStarving()
	
	if not sleeper:HasTag("slurtle") then
		--print ("Too bad he isn't a spider!")
		 inst.components.sleepingbag:DoWakeUp()
	end
	
    if sleeper.components.hunger ~= nil then
        sleeper.components.hunger:DoDelta(inst.hunger_tick, true, true)
        isstarving = sleeper.components.hunger:IsStarving()
    end

    if sleeper.components.sanity ~= nil and sleeper.components.sanity:GetPercentWithPenalty() < 1 then
        sleeper.components.sanity:DoDelta(TUNING.SLEEP_SANITY_PER_TICK * 2, true)
    end

    if not isstarving and sleeper.components.health ~= nil then
        sleeper.components.health:DoDelta(TUNING.SLEEP_HEALTH_PER_TICK, true, inst.prefab, true)
    end

    if sleeper.components.temperature ~= nil then
        if inst.is_cooling then
            if sleeper.components.temperature:GetCurrent() > TUNING.SLEEP_TARGET_TEMP_TENT then
                sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() - TUNING.SLEEP_TEMP_PER_TICK)
            end
        elseif sleeper.components.temperature:GetCurrent() < TUNING.SLEEP_TARGET_TEMP_TENT then
            sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() + TUNING.SLEEP_TEMP_PER_TICK)
        end
    end

    if isstarving then
        inst.components.sleepingbag:DoWakeUp()
    end
end

local function onsleep(inst, sleeper)
    --inst:WatchWorldState("phase", wakeuptest)
    sleeper:ListenForEvent("onignite", onignite, inst)
	--inst.AnimState:PlayAnimation("hit")
	--print ("someone wants to sleep in the den!")
	
	
    --if inst.sleep_anim ~= nil then
        --inst.AnimState:PlayAnimation(inst.sleep_anim, true)
    --end

    if inst.sleeptask ~= nil then
        inst.sleeptask:Cancel()
    end
	
	inst.SoundEmitter:PlaySound("dontstarve/creatures/slurtle/mound_LP", "loop")
	
	--if sleeper:HasTag("spider") then
	--inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
    inst.sleeptask = inst:DoPeriodicTask(TUNING.SLEEP_TICK_PERIOD, onsleeptick, nil, sleeper)
	
	--end
end

local function MakeDenFn()
	return function()
    local inst = CreateEntity()

    inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddMiniMapEntity()
        inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 2)

	inst.hunger_tick = 0
	
	MakeSnowCoveredPristine(inst)

    inst.MiniMapEntity:SetIcon("slurtle_den.png")

    inst.AnimState:SetBank("slurtle_mound")
    inst.AnimState:SetBuild("slurtle_mound")
    inst.AnimState:PlayAnimation("idle", true)

    inst:AddTag("structure")
    inst:AddTag("chewable") -- by werebeaver
    inst:AddTag("cavedweller")
	inst:AddTag("shelter") -- this is to allow cooling during summer.
	inst:AddTag("slurtlehouse")

	inst.mobhouse = true
	
	inst:AddComponent("heater") 
	inst.components.heater.heat = 40
		
    --MakeSnowCoveredPristine(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	-------------------
    --inst:AddComponent("health")
    --inst.components.health:SetMaxHealth(300)
    --inst:ListenForEvent("death", OnKilled)

    -------------------
	
	inst:AddComponent("workable")
	inst:AddComponent("sleepingbag")
	--MakeHauntableWork(inst)
    inst.components.sleepingbag.onsleep = onsleep
    inst.components.sleepingbag.onwake = onwake
    --convert wetness delta to drying rate
    inst.components.sleepingbag.dryingrate = math.max(0, -TUNING.SLEEP_WETNESS_PER_TICK / TUNING.SLEEP_TICK_PERIOD)

	MakeLargeBurnable(inst)
    --V2C: Remove default OnBurnt handler, as it conflicts with
    --explosive component's OnBurnt handler for removing itself
    inst.components.burnable:SetOnBurntFn(nil)
    inst.components.burnable:SetOnIgniteFn(onignite)
    inst.components.burnable:SetOnExtinguishFn(OnExtinguishFn)

    inst:AddComponent("explosive")
    inst.components.explosive:SetOnExplodeFn(OnExplodeFn)
    inst.components.explosive.explosivedamage = 50
    inst.components.explosive.buildingdamage = 15
    inst.components.explosive.lightonexplode = false
	
	inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(3)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)
	
    inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('catcoonden_p')
	inst.components.lootdropper:SetLoot(loot)

    inst:AddComponent("hauntable")
    inst.components.hauntable:SetHauntValue(TUNING.HAUNT_SMALL)
    --inst.components.hauntable:SetOnHauntFn(OnHaunt)
	
	inst.data = {}
	
    --inst:WatchWorldState("isday", OnIsDay)

    --StartSpawning(inst)


        ---------------------
       MakeMediumPropagator(inst)


    inst:AddComponent("inspectable")

    MakeSnowCovered(inst)
	
	inst.OnEntitySleep = OnEntitySleep
        inst.OnEntityWake = OnEntityWake

    return inst
	end
end

return Prefab("slurtlemound_p", MakeDenFn(), assets, prefabs),
	MakePlacer("slurtlemound_p_placer", "slurtle_mound", "slurtle_mound", "idle")
