require "prefabutil"

local assets =
{
    Asset("ANIM", "anim/treasure_chest.zip"),
   Asset("ANIM", "anim/ui_chester_shadow_3x4.zip"),
}

local prefabs =
{
    "collapse_small",
}

local loot =
{
	"cutgrass",
	"cutgrass",
	"boards",
}
local function shake(inst)
    inst.AnimState:PlayAnimation(math.random() > .5 and "move1" or "move2")
    inst.AnimState:PushAnimation("idle")
    inst.SoundEmitter:PlaySound("dontstarve/creatures/monkey/barrel_rattle")
end

local function enqueueShake(inst)
    if inst.shake ~= nil then
        inst.shake:Cancel()
    end
    inst.shake = inst:DoPeriodicTask(GetRandomWithVariance(10, 3), shake)
end

local function pushsafetospawn(inst)
    inst.task = nil
    inst:PushEvent("safetospawn")
end

local function onopen(inst, data)
    if not inst:HasTag("burnt") and (data.doer and data.doer:HasTag("monkey")) then
        inst.AnimState:PlayAnimation("move1")
        inst.SoundEmitter:PlaySound("dontstarve/creatures/monkey/barrel_rattle")
	else
		inst.components.container:Close()
    end
end 

local function onclose(inst)
    if not inst:HasTag("burnt") then
        inst.AnimState:PlayAnimation("move2")
        inst.SoundEmitter:PlaySound("dontstarve/creatures/monkey/barrel_rattle")
    end
end

local function onhammered(inst, worker)
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
    inst.components.lootdropper:DropLoot()
    if inst.components.container ~= nil then
        inst.components.container:DropEverything()
    end
	if inst.components.childspawner ~= nil then
			inst.components.childspawner:ReleaseAllChildren()
	end
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function ongohome(inst, child)
    if child.components.inventory ~= nil then
        child.components.inventory:DropEverything(false, true)
    end
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
        inst.AnimState:PlayAnimation("hit")
        inst.AnimState:PushAnimation("idle", false)
        if inst.components.container ~= nil then
            inst.components.container:DropEverything()
            inst.components.container:Close()
        end
		if inst.components.childspawner ~= nil then
			inst.components.childspawner:ReleaseAllChildren()
		end
    end
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("move2")
    inst.SoundEmitter:PlaySound("dontstarve/creatures/monkey/barrel_rattle")
end

local function onsave(inst, data)
    if inst:HasTag("burnt") or (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
        data.burnt = true
    end
end

local function onload(inst, data)
    if data ~= nil and data.burnt then
        inst.components.burnable.onburnt(inst)
    end
end

local function fn()
    
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddMiniMapEntity()
        inst.entity:AddNetwork()

        inst.MiniMapEntity:SetIcon("monkey_barrel.png")
		
		MakeObstaclePhysics(inst, 1)
		
        inst:AddTag("structure")
		inst:AddTag("shelter")
        inst:AddTag("chest")
		inst:AddTag("monkeychest")
        inst.AnimState:SetBank("barrel")
        inst.AnimState:SetBuild("monkey_barrel")
        inst.AnimState:PlayAnimation("idle", true)
		
		inst:AddComponent("heater") 
		inst.components.heater.heat = 40

        --MakeSnowCoveredPristine(inst)

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst:AddComponent("inspectable")
        inst:AddComponent("container")
        inst.components.container:WidgetSetup("monkeybarrel_p")
		
			inst.components.container.onopenfn = onopen
			inst.components.container.onclosefn = onclose

			
		--[[
		if MOBPVPMODEC == "Enable" then
			inst:AddComponent( "childspawner" )
			inst.components.childspawner:SetRegenPeriod(120)
			inst.components.childspawner:SetSpawnPeriod(30)
			inst.components.childspawner:SetMaxChildren(math.random(3, 4))
			inst.components.childspawner:StartRegen()
			inst.components.childspawner.childname = "monkey"
			inst.components.childspawner:StartSpawning()
			inst.components.childspawner.ongohome = ongohome
			inst.components.childspawner:SetSpawnedFn(shake)
		end]]
		
		
        inst:AddComponent("lootdropper")
		inst.components.lootdropper:SetLoot(loot)
		
        inst:AddComponent("workable")
        inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
        inst.components.workable:SetWorkLeft(3)
        inst.components.workable:SetOnFinishCallback(onhammered)
        inst.components.workable:SetOnWorkCallback(onhit) 

        AddHauntableDropItemOrWork(inst)

        inst:ListenForEvent("onbuilt", onbuilt)
        MakeSnowCovered(inst)   

        MakeSmallBurnable(inst, nil, nil, true)
        MakeMediumPropagator(inst)

        inst.OnSave = onsave 
        inst.OnLoad = onload

        return inst
    end

return Prefab("monkeybarrel_p", fn, assets, prefabs)
   -- MakePlacer("treasurechest_placer", "monkeybarrel_p", "monkeybarrel_p")
