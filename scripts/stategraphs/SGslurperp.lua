require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "taunt"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnDeath(),
}


 local states=
{
	
	State{
        
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
			local hunger = inst.components.hunger:GetPercent()
			local hungerchance = math.random() 
         
            inst.AnimState:PushAnimation("idle_loop", true)
			if hungerchance <= 0.3333 and hunger <= 0.4 then
				inst.sg:GoToState("rumble")
			end
        end,
        
        timeline = 
        {

        },
        
        events=
        {
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle") 
            end),
        },
    },

    State{
        
        name = "rumble",
		tags = {"idle"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_rumble")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/rumble")
        end,
        events=
        {
            EventHandler("animover", function (inst)
                inst.sg:GoToState("idle")
            end),
        }
    }, 

    State{
        name = "taunt",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,
       
        timeline = 
        {
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/taunt") end),
            TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/taunt") end),
            TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/taunt") inst:PerformBufferedAction() end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,
       
        timeline = 
        {
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/taunt") end),
            TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/taunt") end),
            TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/taunt") end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "burp",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.shouldburp = false
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("burp")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/burp")
        end,
		
		timeline =
        {
            TimeEvent(10*FRAMES, function(inst)
				inst:PerformBufferedAction()
             end),
        },
		
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "headslurp",
        tags = {"attack", "busy", "jumping"},

        onenter = function(inst, target)
            inst.AnimState:PlayAnimation("headslurp")
        end,

        timeline =
        {
            TimeEvent(8*FRAMES, function(inst)
                inst.Physics:SetMotorVelOverride(8,0,0)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/jump")
             end),

            TimeEvent(23*FRAMES, function(inst)
                inst.Physics:ClearMotorVelOverride()
                inst.components.locomotor:Stop()
            end),

            TimeEvent(24*FRAMES, function(inst)
                --V2C: Need to revalidate target! Combat target could've
                --     changed after all these frames and state changes!
                local target = inst.components.combat.target
                if target ~= nil and
                    target:IsValid() and
                    inst:IsNear(target, 2) and
                    inst.HatTest ~= nil and
                    inst:HatTest(target) then
                    local oldhat = target.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
                    if oldhat ~= nil then
                        target.components.inventory:DropItem(oldhat)
                    end
                    target.components.inventory:Equip(inst)
                end
            end),
        },

        events =
        {
            --Check attachment eligibility. Either go into hat mode or miss mode.
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("headslurpmiss") 
            end),
        },
    },

    State{
        name = "headslurpmiss",
        tags = {"attack", "busy"},

        onenter = function(inst, target)
            inst.AnimState:PlayAnimation("headslurpmiss")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/miss")

        end,

        events =
        {
            --Go to taunt
            EventHandler("animover", function(inst) 
                --inst.shouldburp gets set in "onequip". This means he's been "feeding" so he should burp.
                if not inst.shouldburp then
                    inst.sg:GoToState("taunt") 
                else
                    inst.sg:GoToState("burp") 
                end
            end),
        },
    },

    State{
        name = "attack",
        tags = {"attack", "busy", "jumping"},
        
        onenter = function(inst, target)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)

            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")
            inst.sg.statemem.target = target

        end,             

        onexit = function(inst)
            inst.SoundEmitter:KillSound("roll_VO")
            inst.SoundEmitter:KillSound("roll_dirt")
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
        end,

        timeline = {
            TimeEvent(20*FRAMES, function(inst)
				--inst:PerformBufferedAction()
                inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/roll_VO", "roll_VO")
                inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/roll_dirt", "roll_dirt")
                inst.Physics:SetMotorVelOverride(20,0,0) 
            end),

            TimeEvent(30*FRAMES, function(inst) 
                              
				inst:PerformBufferedAction()
                inst.SoundEmitter:KillSound("roll_VO")
                inst.SoundEmitter:KillSound("roll_dirt")
                inst.Physics:ClearMotorVelOverride()
                inst.components.locomotor:Stop()
				--end
            end),
      
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },


    State{
        name = "hit",
        tags = {"hit", "busy"},
        
        onenter = function(inst, cb)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/hurt")
        end,
      
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,
		timeline = 
		{ 
			TimeEvent(7*FRAMES, function(inst) inst.Light:Enable(false) end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,
		
		timeline =
		{
			TimeEvent(5*FRAMES, function(inst) inst.Light:Enable(true) end),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
			
           -- inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline =
		{
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/die") end),
            TimeEvent(60*FRAMES,function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/pop")
                inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition())) 
            end),
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.AnimState:PlayAnimation("roll_pre")			
        end,
		
		timeline = 
            {
                TimeEvent(7*FRAMES, function(inst) inst.components.locomotor:WalkForward() end),
            },
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			
			inst.components.locomotor:WalkForward()
                inst.AnimState:PlayAnimation("roll_loop", true)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/roll_VO", "roll_VO")
                inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/roll_dirt", "roll_dirt")
			
        end,
		
		 onexit = function(inst)
                inst.SoundEmitter:KillSound("roll_VO")
                inst.SoundEmitter:KillSound("roll_dirt")
            end,
		
		timeline =
        {
             
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.AnimState:PlayAnimation("roll_pst")               
                inst.components.locomotor:StopMoving()
            
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}
    
	
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
			
            inst:PerformBufferedAction()
        end),
	}, 
	"roll_pst", nil, nil, "idle_loop", "roll_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/die") end),
            TimeEvent(60*FRAMES,function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/pop")
            end),
		},
		
		corpse_taunt =
		{
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/taunt") end),
            TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/taunt") end),
            TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/slurper/taunt") end), 
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "roll_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
CommonStates.AddHopStates(states, false, {pre = "roll_pre", loop = "roll_loop", pst = "roll_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle_loop",
	plank_idle_loop = "idle_loop",
	plank_idle_pst = "roll_pst",
	
	plank_hop_pre = "roll_pre",
	plank_hop = "roll_loop",
	
	steer_pre = "roll_pst",
	steer_idle = "idle_loop",
	steer_turning = "taunt",
	stop_steering = "roll_pst",
	
	row = "roll_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "roll_pst",
	
	leap_pre = "roll_pre",
	leap_loop = "roll_loop",
	leap_pst = "roll_pst",
	
	castspelltime = 10,
})     

return StateGraph("slurperp", states, events, "idle", actionhandlers)

